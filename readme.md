#  ADOTEBICHO

O "AdoteBicho" será um ***sistema web*** que certamente irá ser codificado em **[Laravel](https://laravel.com/)** 
(*framework* PHP) e contará com outras tecnologias auxiliares para o seu desenvolvimento: 
**[jQuery](https://jquery.com/)**, **[Bootstrap 4](https://getbootstrap.com/)**, 
**[MySQL](https://www.mysql.com/)** e [outras]()... Embora ainda seja um projeto 
bastante novo, ele oferece uma visão ampla de uma aplicação simples usando 
essas tecnologias e em breve o mesmo poderá ser colocado em **produção** atendendo as 
espectativas de um possível **cliente** pois, o mesmo não será somente um sistema 
fictício como requisito da disciplina de **Engenharia de Software III**. Sendo assim, o 
projeto (sistema *web*) basicamente possibilitara a um usuário cadastrar um animal 
do tipo cachorro/gato (**doação**), de modo que outros usuários 
possam adotadar um cachorro/gato, para ter mais detalhes da dinâmica do
negócio clique [aqui]().

O projeto começou ser desenvolvido na aula 06 (10/09/2018) em [grupo](#grupo) e 
faz parte da avaliação da disciplina de **Engenharia de Software III** do 
4° semestre da 
**[Faculdade de Tecnologia Senac Pelotas (RS)](https://www.senacrs.com.br/unidades.asp?unidade=78)**, 
assim como outras atividades em paralelo que os membros do grupo tenham que realizar, como porvas, outros trabalhos e etc. 
O projeto conta também com a orientação do prof. 
**[Angelo Luz](https://gitlab.com/angelogl)** e o andamento do mesmo em breve poderá ser 
acompanhado (visto) no repositório do **[Heroku](https://www.heroku.com/)** ou outro!

🚧 **Situação atual do projeto:** em construção/desenvolvimento!

Sinta-se livre para [clonar](#clonar) ou [baixar](#clonar) o código-fonte do projeto.

## Pré-requisitos

O **[Laravel](https://laravel.com)** assim como outros *frameworks* necessitam 
de alguns [requisitos de sistema](https://laravel.com/docs/5.6/installation#installation). 
O *site* do **Laravel**
sugere que para desenvolver aplicações em **Laravel** o ideal é usar a máquina virtual 
**[Laravel Homestead](https://laravel.com/docs/5.6/homestead)** pois, ela atende a 
todos esses [requisitos de sistema](https://laravel.com/docs/5.6/installation#installation). Porém, caso você não use o **Homestead** o 
recomendado seria o uso da ferramenta **[XAMPP](https://www.apachefriends.org/pt_br/index.html)** ou similares 
pois, é fácil de instalar e possui todo o ambiente de desenvolvimento necessário 
para desenvolver aplicações em **Laravel** ou "rodar" aplicações baixadas 
(prontas/desenvolvimento) como essa!

Outra ferramenta que faz parte dos [requisitos de sistema](https://laravel.com/docs/5.6/installation#installation) 
é o **[Composer](https://getcomposer.org/)** 
que serve para gerenciar dependências. Nesse **README** não será mostrado como 
instalar essas ferramentas então, fique a vontade em clicar nos *links* fornecidos 
anteriormente para obter a documentação dos mesmos junto aos seus *sites* oficiais.

⚠ **ATENÇÃO, as ferramentas, comandos, linguagens (*framework*) e etc. requerem conhecimentos prévios!**

## Índice

* [Clonar](#clonar)
* [How to (como usar)](#uso)
* [O grupo](#grupo)
* [Documentação](#documentação)
* [Licença](#licença)

## Clonar

Para efetuar o clone do projeto usando os *links* disponíveis no repositório 
GitLab é preciso possuir o *software* Git instalado na sua máquina. 
Para saber como instalar e usar a ferramenta Git acesse o *site* do 
Git: <https://git-scm.com/doc>. Caso já saiba como fazer use os comandos abaixo:

**Com SSH:**

```bash
$ git clone git@gitlab.com:ES-III/AdoteDog.git
```

ou 

**Com HTTPS:**

```bash
$ git clone https://gitlab.com/ES-III/AdoteDog.git
```

ou você pode baixar o projeto (*download*): [zip](https://gitlab.com/ES-III/AdoteDog/-/archive/master/AdoteDog-master.zip),
[tar.gz](https://gitlab.com/ES-III/AdoteDog/-/archive/master/AdoteDog-master.tar.gz),
[tar.bz2](https://gitlab.com/ES-III/AdoteDog/-/archive/master/AdoteDog-master.tar.bz2) e 
[tar](https://gitlab.com/ES-III/AdoteDog/-/archive/master/AdoteDog-master.tar).

## Uso

**ANTES DE USAR:** leia os [pré-requisitos](#pré-requisitos).

Após ter clonado/baixado o projeto, execute os seguintes comandos listados abaixo 
no diretório do projeto usando as ferramentas de sua preferência (cmd Windows, terminal do Linux, Git ou IDE):

* `composer install`: serve para ler o arquivo `composer.lock` e instalar as dependências listadas nesse arquivo;
* `composer dump`: serve para recarregar a lista de **classes**, **pacotes** e **bibliotecas** que estão dentro do seu projeto no arquivo de **autoload**;
* `cp .env.example .env`: serve para criar o arquivo `.env` caso o projeto não possua (no arquivo é preciso configurar o banco de dados);
* `php artisan key:generate`: serve para gerar uma chave do tipo de segurança para o seu arquivo `.env`;
* `php artisan migrate`: serve para criar as tabelas (arquivos) compostos no projeto no **banco de dados** que estará rodando no momento;
* `php artisan db:seed`: serve para **inserir** registros nas tabelas criadas anteriormente;
* `php artisan serve`: e por fim, esse comando serve para iniciar a aplicação que pode ser visualizada no **navegador** (*browser*).

## Grupo

O nosso grupo conta com os seguintes membros (mantenedores) e cada um é responsável 
por uma função dentro do processo de desenvolvimento conforme é mostrado abaixo:

<a href="https://gitlab.com/grazzianofagundes">
            <img src="https://image.ibb.co/cAZxcK/Grazziano.jpg" /></a>
<a href="https://gitlab.com/Rafael_Rossales">
            <img src="https://image.ibb.co/kOPr4z/Rafael.jpg" /></a>
<a href="https://gitlab.com/rafaelcalearo">
            <img src="https://image.ibb.co/fKQyjz/Calearo.jpg" /></a>
<a href="https://gitlab.com/mborges93">
            <img src="https://image.ibb.co/ebcB4z/Matheus.jpg" /></a>
<a href="https://gitlab.com/LucasXx">
            <img src="https://image.ibb.co/cdVvxK/Lucas.jpg" /></a>
<a href="https://gitlab.com/thaua97">
            <img src="https://image.ibb.co/gMT9Pz/Thaua.jpg" /></a>
<a href="https://gitlab.com/mig.mdasilva">
            <img src="https://image.ibb.co/i7Q0HU/Miguel.jpg" /></a>

## Documentação

A documentação com maior clareza se encontra na **[wiki](https://gitlab.com/ES-III/AdoteDog/wikis/home)** desse repositório.

## Licença

A aplicação conta com a seguinte licença de uso: **[MIT](https://opensource.org/licenses/MIT).**
