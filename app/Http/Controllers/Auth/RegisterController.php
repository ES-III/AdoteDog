<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            ///^[0-9`~!@#$%^&*()_\-+={}[\]\\\|:;"\'<>,\.\?\/]+$/' PADRAO
            //array('required', 'min:3', 'max:30', 'regex:/^[0-9]+'),
            //'required|string|min:3|max:30',
            'name' => 'required|string|min:3|max:30',
            'sobrenome' => 'required|string|min:3|max:30',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ], 
        [
            'name.required' => '* O nome é obrigatório!',                   
            'name.min' => '* O nome deve ser maior que 3 letras!',
            'name.max' => '* O nome deve ter no máximo 30 letras!',                       
            'sobrenome.required' => '* O sobrenome é obrigatório!',
            'sobrenome.min' => '* O sobrenome deve ser maior que 3 letras!',
            'sobrenome.max' => '* O sobrenome deve ter no máximo 30 letras!',  
            'email.required' => '* O e-mail é obrigatório!', 
            'email.email' => '* O e-mail não é válido!', 
            'email.unique' => '* O e-mail já está cadastrado!', 
            'password.required' => '* A senha é obrigatória!', 
            'password.min' => '* A senha deve ser maior que 6 digitos!',
            'password.confirmed' => '* As senhas não são iguais!',
            'password_confirmation.required' => '* Senha de confirmação é obrigatória!',            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data['name'] = trim($data['name']) . " " . trim($data['sobrenome']);      
        return User::create([            
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
